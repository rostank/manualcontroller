#include <chrono>
#include <exception>
#include <functional>
#include <memory>
#include <string>
#include <stdexcept>
#include <thread>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "tank_interface/msg/relative_move_command.hpp"
#include "tank_interface/msg/turn_command.hpp"

using namespace std::chrono_literals;

class TankController : public rclcpp::Node {
 public:
  TankController(std::string tank_name) :
      Node("TankController"),
      tank_name_(tank_name),
      user_input_thread_(&TankController::iterate, this)
  {
    move_publisher_ = this->create_publisher<tank_interface::msg::RelativeMoveCommand>(
        "/move", 10);
    turn_publisher_ = this->create_publisher<tank_interface::msg::TurnCommand>(
        "/turn", 10);
  }

  void iterate() {
    while(true) {
      std::cout << " > ";
      std::string command;
      std::getline(std::cin, command);
      process_command(command);
    }
  }

  void process_command(std::string command) {
    auto tokenized_command = tokenize_command(command);
    if (is_move_command(tokenized_command)) {
      handle_move_command(tokenized_command);
    } else if (is_move_command(tokenized_command)) {
      handle_move_command(tokenized_command);
    } else if (is_turn_command(tokenized_command)) {
      handle_turn_command(tokenized_command);
    } else if (is_exit_command(tokenized_command)) {
      exit();
    } else {
      std::cout << "Unknown command" << std::endl;
    }
  }

 private:
  std::vector<std::string> tokenize_command(std::string command) {
    std::stringstream command_stream(command);
    
    std::vector<std::string> tokenized_command;
    std::string token;
    
    while( command_stream >> token ) {
      tokenized_command.push_back(token);
    }
    return tokenized_command;
  }

  bool is_move_command(std::vector<std::string> command) {
    return (
        (lower(command[0]) == "move") ||
        (lower(command[0]) == "forward") ||
        (lower(command[0]) == "backward"));
    }

  void handle_move_command(std::vector<std::string> command) {
    if (lower(command[0]) == "move") {
      if (command.size() != 3) {
        std::cout << "Invalid use of 'move', expected move x y, Got: ";
        for( const auto token : command ) {
          std::cout << token << " ";
        }
        std::cout << std::endl;
        return;
      }
      auto move = tank_interface::msg::RelativeMoveCommand();
      move.move_x = std::stof(command[1]);
      move.move_y = std::stof(command[2]);
      publish_move(move);
    } else if (lower(command[0]) == "forward") {
      if (command.size() != 2) {
        std::cout << "Invalid use of 'forward', expected forward x, Got: ";
        for( const auto token : command ) {
          std::cout << token << " ";
        }
        std::cout << std::endl;
        return;
      }
      auto move = tank_interface::msg::RelativeMoveCommand();
      move.move_x = 0;
      move.move_y = std::stof(command[1]);
      publish_move(move);
    } else if (lower(command[0]) == "backward") {
      if (command.size() != 2) {
        std::cout << "Invalid use of 'backward', expected backward x, Got: ";
        for( const auto token : command ) {
          std::cout << token << " ";
        }
        std::cout << std::endl;
        return;
      }
      auto move = tank_interface::msg::RelativeMoveCommand();
      move.move_x = 0;
      move.move_y = -std::stof(command[1]);
      publish_move(move);
    } else {
      std::cout << "Unknown move command" << std::endl;
    }
  }

  bool is_turn_command(std::vector<std::string> command) {
    return (
        (lower(command[0]) == "turn") ||
        (lower(command[0]) == "left") ||
        (lower(command[0]) == "right"));
  }

  void handle_turn_command(std::vector<std::string> command) {
    if ( !( (lower(command[0]) == "turn" ) || (lower(command[0]) == "left") || (lower(command[0]) == "right"))) {
      std::cout << "Unknown turn command" << std::endl;
      return;
    }
    if (command.size() != 2) {
      std::cout << "Invalid use of '" << command[0] << "': " << 
          "expected " << command[0] << " degrees, Got: ";
      for( const auto token : command ) {
        std::cout << token << " ";
      }
      std::cout << std::endl;
      return;
    }
    auto turn = tank_interface::msg::TurnCommand();
    turn.degrees = std::stof(command[1]);
    
    if (lower(command[0]) == "right") {
      turn.degrees = -turn.degrees;
    }
    
    publish_turn(turn);
  }
  bool is_exit_command(std::vector<std::string> command) {
    return (
        (lower(command[0]) == "exit") ||
        (lower(command[0]) == "quit"));
  }
  void exit() {
    throw std::logic_error("Exiting");
  }

  std::string lower(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(), [](unsigned char c) { return std::tolower(c); });
    return word;
  }

  void publish_move(tank_interface::msg::RelativeMoveCommand move) {
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", (std::to_string(move.move_x) + ", " + std::to_string(move.move_y)).c_str());
    move_publisher_->publish(move);
  }

  void publish_turn(tank_interface::msg::TurnCommand turn) {
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", (std::to_string(turn.degrees).c_str()));
    turn_publisher_->publish(turn);
  }

  rclcpp::Publisher<tank_interface::msg::RelativeMoveCommand>::SharedPtr move_publisher_;
  rclcpp::Publisher<tank_interface::msg::TurnCommand>::SharedPtr turn_publisher_;
  std::string tank_name_;
  std::thread user_input_thread_;
};


int main(int argc, char * argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TankController>("tank1"));
  rclcpp::shutdown();
  return 0;
}
